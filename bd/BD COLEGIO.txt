create table nivel(id_nivel varchar(50) not null,  PRIMARY KEY (`id_nivel`));
create table grado(id_grado varchar(50) not null, id_nivel varchar(50), PRIMARY KEY (`id_grado`), KEY `id_nivel` (`id_nivel`));
ALTER TABLE `grado`
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`id_nivel`) REFERENCES `nivel` (`id_nivel`) ON DELETE CASCADE ON UPDATE CASCADE;

  
create table carrera(id_carrera varchar(50) not null, descripcion_carrera varchar(100), id_nivel varchar(50), PRIMARY KEY(`id_carrera`), KEY `id_nivel` (`id_nivel`));
ALTER TABLE `carrera`
  ADD CONSTRAINT `carrera_ibfk_1` FOREIGN KEY (`id_nivel`) REFERENCES `nivel` (`id_nivel`) ON DELETE CASCADE ON UPDATE CASCADE;

  
create table pensum(id_pensum varchar(50) not null, id_nivel varchar(50) not null, id_grado varchar(50) not null, id_carrera varchar(50), fecha_pensum TIMESTAMP DEFAULT CURRENT_TIMESTAMP  
ON UPDATE CURRENT_TIMESTAMP, PRIMARY KEY (`id_pensum`),  KEY `id_nivel` (`id_nivel`), KEY `id_grado` (`id_grado`), KEY `id_carrera` (`id_carrera`) )
ALTER TABLE `pensum`
  ADD CONSTRAINT `pensum_nivel_ibfk_1` FOREIGN KEY (`id_nivel`) REFERENCES `nivel` (`id_nivel`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `pensum`
  ADD CONSTRAINT `pensum_grado_ibfk_1` FOREIGN KEY (`id_grado`) REFERENCES `grado` (`id_grado`) ON DELETE CASCADE ON UPDATE CASCADE;
  
  ALTER TABLE `pensum`
  ADD CONSTRAINT `pensum_grado_ibfk_1` FOREIGN KEY (`id_grado`) REFERENCES `grado` (`id_grado`) ON DELETE CASCADE ON UPDATE CASCADE;
  
  
  
    create table alumno(id_alumno int auto_increment, carnet varchar(50), nombre varchar(100) not null, apellido varchar(100) not null, fecha_nacimiento varchar(15), nombre_encargado varchar(50) not null, direccion  varchar(100) not null,  telefono int,  id_pensum varchar(50) not null, contrasena varchar(50), usuario varchar(50), fecha_creacion_alumno TIMESTAMP DEFAULT CURRENT_TIMESTAMP  ON UPDATE CURRENT_TIMESTAMP, primary key(id_alumno), KEY `id_pensum` (`id_pensum`));
  
  ALTER TABLE `alumno`
  ADD CONSTRAINT `alumno_pensum1_ibfk_1` FOREIGN KEY (`id_pensum`) REFERENCES `pensum` (`id_pensum`) ON DELETE CASCADE ON UPDATE CASCADE;
  
  create table curso(id_curso varchar(50) not null, nombre_curso varchar(50) not null, grado varchar(50) not null, fecha_creacion_curso TIMESTAMP DEFAULT CURRENT_TIMESTAMP  ON UPDATE CURRENT_TIMESTAMP, primary key(id_curso), KEY `grado` (`grado`))
   ALTER TABLE `curso`
  ADD CONSTRAINT `curso_grado_ibfk_1` FOREIGN KEY (`grado`) REFERENCES `grado` (`id_grado`) ON DELETE CASCADE ON UPDATE CASCADE;
  
  create table profesor(id_profesor int auto_increment, nombre_profesor varchar(50) not null, apellido_profesor varchar(50) not null, dpi bigint not null, fecha_nacimiento date, telefono int, correo varchar(50), direccion varchar(100), contrasena varchar(50), primary key(id_profesor))
  
CREATE TRIGGER usuario_profesor
BEFORE INSERT ON profesor
FOR EACH ROW
insert into usuarios(id_usu, nomb_usu, pass_usu, id_area) values (NEW.correo, NEW.nombre_profesor, NEW.contrasena, 'profesor')

select concat(left(nombre,1), left(apellido,10)) as carnet from alumno


CREATE TRIGGER usuario_alumno
BEFORE INSERT ON alumno
FOR EACH ROW
insert into usuarios(id_usu, nomb_usu, pass_usu, id_area) values (NEW.usuario, NEW.nombre, NEW.contrasena, 'alumno')
