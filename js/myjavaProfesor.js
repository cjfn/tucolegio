$(function(){
	
	
	
        
        $('#nuevo-profesor').on('click',function(){
		$('#formulario')[0].reset();
		$('#pro').val('Registro');
		$('#edi').hide();
		$('#reg').show();
		$('#registra-profesor').modal({
			show:true,
			backdrop:'static'
		});
	});
        
	$('#bs-profesor').on('keyup',function(){
		var dato = $('#bs-profesor').val();
		var url = '../php/busca_profesor.php';
		$.ajax({
		type:'POST',
		url:url,
		data:'dato='+dato,
		success: function(datos){
			$('#agrega-registros').html(datos);
		}
	});
	return false;
	});
	
});


function modificaProfesor(){
	var url = '../php/modifica_Profesor.php';
	$.ajax({
		type:'POST',
		url:url,
		data:$('#formulario').serialize(),
		success: function(registro){
			if ($('#pro').val() === 'Registro'){
			$('#formulario')[0].reset();
			$('#mensaje').addClass('bien').html('Registro completado con exito').show(200).delay(2500).hide(200);
			$('#agrega-registros').html(registro);
			return false;
			}else{
			$('#mensaje').addClass('bien').html('Edicion completada con exito').show(200).delay(2500).hide(200);
			$('#agrega-registros').html(registro);
			return false;
			}
		}
	});
	return false;
}

function eliminarProfesor(id){
	var url = '../php/elimina_Profesor.php';
	var pregunta = confirm('¿Esta seguro de eliminar este Profesor?');
	if(pregunta===true){
		$.ajax({
		type:'POST',
		url:url,
		data:'id='+id,
		success: function(registro){
			$('#agrega-registros').html(registro);
			return false;
		}
	});
	return false;
	}else{
		return false;
	}
}

function editarProfesor(id){
	$('#formulario')[0].reset();
	var url = '../php/edita_Profesor.php';
		$.ajax({
		type:'POST',
		url:url,
		data:'id='+id,
		success: function(valores){
				var datos = eval(valores);
				$('#reg').hide();
				$('#edi').show();
				$('#pro').val('Edicion');
				$('#id-Profesor').val(id);
				$('#nombre_curso').val(datos[0]);
				$('#grado').val(datos[1]);
				
				$('#registra-curso').modal({
					show:true,
					backdrop:'static'
				});
			return false;
		}
	});
	return false;
}



function editarCursos(id){
	$('#formulario')[0].reset();
	var url = '../php/edita_cursos.php';
		$.ajax({
		type:'POST',
		url:url,
		data:'id='+id,
		success: function(valores){
				var datos = eval(valores);
				$('#reg').hide();
				$('#edi').show();
				$('#pro').val('Edicion');
				$('#id_curso').val(id);
				$('#nombre_curso').val(datos[0]);
				$('#grado').val(datos[1]);
                $('#registra-curso').modal({
					show:true,
					backdrop:'static'
				});
			return false;
		}
	});
	return false;
}
