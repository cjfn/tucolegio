
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Inventario</title>

<link href="css/estilos.css" rel="stylesheet">
<script src="js/jquery.js"></script>
<script src="js/myjava.js"></script>
<link href="bootstrap/css/bootstrap.css" rel="stylesheet">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="bootstrap/css/bootstrap-theme.css" rel="stylesheet">
<link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="bootstrap/js/bootstrap.js"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

</head>
<body>
<br />
<br />



    <header>
	
<div class="navbar navbar-inverse navbar-fixed-top">

	
    	<div class = "container">
        <a href="#" class="navbar-brand"> DS Informatic </a>
        <button class="navbar-toggle" data-toggle="collapse" data-target=".navHeaderCollapse">

        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
   
                        
        </button>
        <div class ="collapse navbar-collapse navHeaderCollapse">
        	
            <ul class = "nav navbar-nav navbar-right">
            <li class = "active"> <a href="index.php">Home</a></li>
          
            	<li class ="dropdown">
                <a href="Servicios.php" class = "dropdown-toggle" data-toggle="dropdown"> Nuestros servicios</a>
                
                <ul class="dropdown-menu">
                <li> <a href="Aplicaciones.php">Desarrollo Web</a>
				</li>
				 <li> <a href="Vistas/login.php">Tu Colegio</a></li> 
                <li> <a href="Web.php">Aplicaciones Moviles </a></li>
                <li> <a href="Distribuidores.php">Hardware y soluciones</a></li>

                          
                </ul>
                </li>
          
          <li>
           
                <a href="SobreNosotros.php" > Sobre Nosotros </a>
            </li>
                
            
         <li> <a href="Contacto.php"> Contacto </a></li>
                          
                </ul>                                   
            </ul>
            
        </div>
    </div>

	
	
	</header
	
      
       
<?php
include_once('php/db.php');

$query  = "select * from images order by id desc limit 6";
$res    = mysqli_query($connection,$query);
$count  =   mysqli_num_rows($res);

$slides='';
$Indicators='';
$counter=0;

    while($row=mysqli_fetch_array($res))
    {

        $title = $row['title'];
        $desc = $row['desc'];
        $image = $row['image'];
        if($counter == 0)
        {
            $Indicators .='<li data-target="#carousel-example-generic" data-slide-to="'.$counter.'" class="active"></li>';
            $slides .= '<div class="item active">
            <img src="recursos/'.$image.'" alt="'.$title.'" />
            <div class="carousel-caption">
              <h3>'.$title.'</h3>
              <p>'.$desc.'.</p>         
            </div>
          </div>';

        }
        else
        {
            $Indicators .='<li data-target="#carousel-example-generic" data-slide-to="'.$counter.'"></li>';
            $slides .= '<div class="item">
            <img src="recursos/'.$image.'" alt="'.$title.'" />
            <div class="carousel-caption">
              <h3>'.$title.'</h3>
              <p>'.$desc.'.</p>         
            </div>
          </div>';
        }
        $counter++;
    }

?>
<br/>
<br/>
<br/>
<br/>

<div class="container" style="width: 730px;">
      <h2>Dynamic Image Slider</h2><span style="float: right;margin-top: -30px;"><a href="php/addnew.php">Add More Images</a></span>
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
         <?php echo $Indicators; ?>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
        <?php echo $slides; ?>  
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
      </div>
    </div>          

	
	
    <section>
    
    
    
    
    
    <!-- MODAL PARA EL REGISTRO DE VACUNAS-->
    <div class="modal fade" id="registra-vacuna" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="myModalLabel"><b>Registra Vacuna</b></h4>
            </div>
            <form id="formulario1" class="formulario" onsubmit="return modificaVacuna();">
            <div class="modal-body">
				<table border="0" width="100%">
               		 <tr>
                        <td colspan="2"><input type="text" required="required" readonly="readonly" id="id-mascota" name="id-mascota" readonly="readonly" style=" height:25px;"/></td>
                    </tr>
                     <tr>
                    	<td width="150">Proceso: </td>
                        <td><input type="text" required="required" readonly="readonly" id="pro" name="pro"/></td>
                    </tr>
                	<tr>
                    	<td>Nombre de Vacuna: </td>
                        <td><input type="text" required="required" name="nombre" id="nombre" maxlength="100"/></td>
                    </tr>
           
                    <tr>
                    	<td>Tratamiento vacuna: </td>
                        <td><input type="text" required="required" name="tratamiento" id="tratamiento"/></td>
                    </tr>
                    
                    <tr>
                    	<td colspan="2">
                        	<div id="mensaje"></div>
                        </td>
                    </tr>
                </table>
            </div>
            
            <div class="modal-footer">
            	<input type="submit" value="Registrar" class="btn btn-success" id="reg"/>
                <input type="submit" value="Editar" class="btn btn-warning"  id="edi"/>
            </div>
            </form>
          </div>
        </div>
      </div>
      <footer>

<center>
  
     
    <i></i><br />

<i> contacto: cjfn10101@gmail.com</i><br />
<h6>Designed by:<img src="recursos/flash.png" widt="50px" height="50px"/></h6>
</center>

</footer>

</body>
</html>
